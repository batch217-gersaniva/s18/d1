// console.log("Hello World");

// [SECTION] Functions

// Parameters and Arguments

// function printInput(){
// 	let nickname = prompt("Enter your nickname:");
// 	console.log("Hi, " + nickname);
// }

// printInput();

// This function has parameter and argument
function printName(name){      //name ---> parameter
	console.log("My name is " + name)
}

printName("April");  // this is called argument

// 1. printName("Juana"); >>ARGUMENT
// 2. will be stored to our PARAMETER  >> function printName(name)
// 3. the information/data stored in a Parameter can be used to the code block inside a function.


let sampleVariable = "Yui";

printName(sampleVariable);

// Function

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remaider of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(689);


// Functions as Arguments

// 
// 
// 

function argumentFunction(){
	console.log("This function was passed as an argument before the message.")
}

function invokeFunction(argumentFunction){
	argumentFunction();
}


// 
// 
// 

invokeFunction(argumentFunction);

// 
console.log(argumentFunction);


// Multiple Parameters

function createFullName(firstName, middleName, lastName){
	console.log(firstName + '' + middleName + '' + lastName);
}

createFullName("April Marie ", "Sobejano ", "Gersaniva")


// Multiple parameter using stored data in a variable
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName,  middleName,  lastName);

//Parameter names

function printFullName(middleName, firstName, lastName){
	console.log(firstName + ' ' + middleName + ' ' + lastName);

}
printFullName("Juan", "Dela", "Cruz")

//[SECTION] Return Statement
// 

function returnFullName(firstName, middleName, lastName){
	console.log("Test console");
	return firstName + ' ' + middleName + ' ' + lastName;
	console.log("This will not be listed in the console");
}

let completeName = returnFullName("Juan", "Dela", "Cruz");
console.log(completeName);

// 

// 

console.log(returnFullName(firstName, middleName, lastName));

// 

function returnAddress(city, country){
	let fullAddress = city + ", " + country;
	return fullAddress;
	console.log("This will not be displayed");

}

let myAddress = returnAddress("Cebu City", "Philippines");
console.log(myAddress);

// 

function printPlayerInfo(username, level, job){
	console.log("Username: " + username);
	console.log("Level: " + level);
	console.log("Job: " + job);

	return "Username" + username
}

let user1 = printPlayerInfo("Knight_white", 95, "Paladin");
console.log(user1);
